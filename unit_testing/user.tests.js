const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = require("chai").expect;
const app = require("../app");

chai.use(chaiHttp);
chai.should();

describe("User", () => {
  describe("1.Send a message", () => {
    it("Should POST message", (done) => {
      chai
        .request(app)
        .post("/api/sendMessage")
        .send({
          sent_from: "085156960070",
          sent_to: "085156961171",
          content_message: "Hai Jhone?",
        })
        .end((err, res) => {
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.statusText).to.equal("success");
          expect(res.body.message).to.equal("Send message success.");
          expect(res.body).to.have.property("result");
          expect(res.body.result).to.have.property("from");
          expect(res.body.result).to.have.property("to");
          expect(res.body.result).to.have.property("content_message");
          done();
        });
    });
  });

  describe("2.List all message", () => {
    it("Should GET all message", (done) => {
      chai
        .request(app)
        .get("/api/listAllMessage")
        .end((err, res) => {
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.statusText).to.equal("success");
          expect(res.body.message).to.equal("List all message success.");
          expect(res.body).to.have.property("result");
          expect(res.body.result[0]).to.have.property("id");
          expect(res.body.result[0]).to.have.property("time");
          expect(res.body.result[0]).to.have.property("from");
          expect(res.body.result[0]).to.have.property("to");
          expect(res.body.result[0]).to.have.property("content_message");
          done();
        });
    });
  });

  describe("3.Reply a message", () => {
    it("Should POST a message", (done) => {
      chai
        .request(app)
        .post("/api/replyMessages")
        .send({
          reply_from: "085156961171",
          reply_to: "085156960070",
          content_message: "Hai kam.. apa kabar?",
        })
        .end((err, res) => {
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.statusText).to.equal("success");
          expect(res.body.message).to.equal("Reply message success.");
          expect(res.body).to.have.property("result");
          expect(res.body.result).to.have.property("from");
          expect(res.body.result).to.have.property("to");
          expect(res.body.result).to.have.property("content_message");
          done();
        });
    });
  });

  describe("4.List all message perId", () => {
    it("Should GET all message perId", (done) => {
      chai
        .request(app)
        .get("/api/listAllMessagesId/1")
        .end((err, res) => {
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.statusText).to.equal("success");
          expect(res.body.message).to.equal("List all message per id success.");
          expect(res.body).to.have.property("result");
          expect(res.body.result).to.have.property("unread_count");
          expect(res.body.result.data[0]).to.have.property("id");
          expect(res.body.result.data[0]).to.have.property("time");
          expect(res.body.result.data[0]).to.have.property("reply_from");
          expect(res.body.result.data[0]).to.have.property("to");
          expect(res.body.result.data[0]).to.have.property("last_message");
          done();
        });
    });
  });
});
