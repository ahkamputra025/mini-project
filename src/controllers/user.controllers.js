const {
  sendMessageHelper,
  listAllMessagesHelper,
  replyMessagesHelper,
  listAllMessagesIdHelper,
} = require("../helper/user.helper");
const method = {};

method.sendMessage = async (req, res) => {
  try {
    const resData = await sendMessageHelper(req.body);

    if (
      resData === "Message sender number does not exist." ||
      resData === "Message recipient number does not exist."
    )
      return res.status(400).json({
        statusCode: 400,
        statusText: "failed",
        message: resData,
      });

    res.status(200).json({
      statusCode: 200,
      statusText: "success",
      message: "Send message success.",
      result: resData,
    });
  } catch (error) {
    res.status(400).send(error.message);
  }
};

method.listAllMessages = async (req, res) => {
  try {
    const resData = await listAllMessagesHelper();
    if (resData === "No message activity yet.")
      return res.status(400).json({
        statusCode: 400,
        statusText: "failed",
        message: resData,
      });

    res.status(200).json({
      statusCode: 200,
      statusText: "success",
      message: "List all message success.",
      result: resData,
    });
  } catch (error) {
    res.status(400).send(error.message);
  }
};

method.replyMessages = async (req, res) => {
  try {
    const resData = await replyMessagesHelper(req.body);

    if (
      resData === "Message sender number does not exist." ||
      resData === "Message recipient number does not exist." ||
      resData === "You cannot reply to this conversation."
    )
      return res.status(400).json({
        statusCode: 400,
        statusText: "failed",
        message: resData,
      });

    res.status(200).json({
      statusCode: 200,
      statusText: "success",
      message: "Reply message success.",
      result: resData,
    });
  } catch (error) {
    res.status(400).send(error.message);
  }
};

method.listAllMessagesId = async (req, res) => {
  try {
    const resData = await listAllMessagesIdHelper(req.params);

    if (resData === "Id not found." || resData === "No message activity yet.")
      return res.status(400).json({
        statusCode: 400,
        statusText: "failed",
        message: resData,
      });

    res.status(200).json({
      statusCode: 200,
      statusText: "success",
      message: "List all message per id success.",
      result: resData,
    });
  } catch (error) {
    res.status(400).send(error.message);
  }
};

module.exports = method;
