const express = require("express");
const router = express.Router();

const {
  sendMessage,
  listAllMessages,
  replyMessages,
  listAllMessagesId,
} = require("../controllers/user.controllers");

router.post("/sendMessage", sendMessage);
router.get("/listAllMessage", listAllMessages);
router.post("/replyMessages", replyMessages);
router.get("/listAllMessagesId/:id", listAllMessagesId);

module.exports = router;
