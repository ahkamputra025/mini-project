const { users, messages } = require("../database/models");
const { Op } = require("sequelize");
const method = {};

method.sendMessageHelper = async (sourceData) => {
  const dtsent_from = await users.findOne({
    where: { number: sourceData.sent_from },
  });
  if (dtsent_from === null) return "Message sender number does not exist.";

  const dtsent_to = await users.findOne({
    where: { number: sourceData.sent_to },
  });
  if (dtsent_to === null) return "Message recipient number does not exist.";

  const newMessage = await messages.create({
    date: new Date(),
    sent_from: `${dtsent_from.id},${dtsent_from.name}`,
    sent_to: `${dtsent_to.id},${dtsent_to.name}`,
    content_message: sourceData.content_message,
    status: false,
  });

  const resHelpersendMessage = {
    from: dtsent_from.name,
    to: dtsent_to.name,
    content_message: newMessage.content_message,
  };
  return resHelpersendMessage;
};

method.listAllMessagesHelper = async () => {
  const sourceData = await messages.findAll({
    order: [["id", "DESC"]],
  });
  if (sourceData[0] === undefined) return "No message activity yet.";

  const mapData = sourceData.map((doc) => {
    let contain = {};
    const splitFrom = doc.sent_from.split(","),
      splitTo = doc.sent_to.split(",");

    const date = new Date(doc.date),
      hours = "0" + date.getHours(),
      minutes = "0" + date.getMinutes(),
      formatTime = hours.substr(-2) + ":" + minutes.substr(-2);

    contain = {
      id: doc.id,
      time: formatTime,
      from: splitFrom[1],
      to: splitTo[1],
      content_message: doc.content_message,
    };

    return contain;
  });
  return mapData;
};

method.replyMessagesHelper = async (sourceData) => {
  const dtreply_from = await users.findOne({
    where: { number: sourceData.reply_from },
  });
  if (dtreply_from === null) return "Message sender number does not exist.";

  const dtreply_to = await users.findOne({
    where: { number: sourceData.reply_to },
  });
  if (dtreply_to === null) return "Message recipient number does not exist.";

  const mainData = await messages.findAll({
    where: {
      [Op.and]: [
        { sent_from: `${dtreply_to.id},${dtreply_to.name}` },
        { sent_to: `${dtreply_from.id},${dtreply_from.name}` },
      ],
    },
  });

  if (mainData[0] === undefined)
    return "You cannot reply to this conversation.";

  await messages.create({
    date: new Date(),
    sent_from: `${dtreply_from.id},${dtreply_from.name}`,
    sent_to: `${dtreply_to.id},${dtreply_to.name}`,
    content_message: sourceData.content_message,
    status: false,
  });
  for (let i = 0; i < mainData.length; i++) {
    await messages.update({ status: true }, { where: { id: mainData[i].id } });
  }

  const replyMessagesHelper = {
    from: dtreply_from.name,
    to: dtreply_to.name,
    content_message: sourceData.content_message,
  };
  return replyMessagesHelper;
};

method.listAllMessagesIdHelper = async (source) => {
  const getUser = await users.findOne({ where: { id: source.id } });
  if (getUser === null) return "Id not found.";

  const sourceData = await messages.findAll({
    where: { sent_from: `${getUser.id},${getUser.name}` },
    order: [["id", "DESC"]],
  });
  if (sourceData[0] === undefined) return "No message activity yet.";

  let pushData = [];
  for (let i = 0; i < sourceData.length; i++) {
    const getData = await messages.findOne({
      where: {
        [Op.and]: [
          { sent_from: sourceData[i].sent_from },
          { sent_to: sourceData[i].sent_to },
        ],
      },
      order: [["id", "DESC"]],
    });

    const getData1 = await messages.findOne({
      where: {
        [Op.and]: [
          { sent_from: sourceData[i].sent_to },
          { sent_to: sourceData[i].sent_from },
        ],
      },
      order: [["id", "DESC"]],
    });

    if (getData1 === null) {
      pushData.push(getData);
    } else {
      pushData.push(getData, getData1);
    }
  }

  const removeDuplicate = Array.from(
    pushData.reduce((m, t) => m.set(t.id, t), new Map()).values()
  );

  const mapData = removeDuplicate.map((doc) => {
    let contain = {};
    const date = new Date(doc.date),
      hours = "0" + date.getHours(),
      minutes = "0" + date.getMinutes(),
      formatTime = hours.substr(-2) + ":" + minutes.substr(-2);

    const splitFrom = doc.sent_from.split(","),
      splitTo = doc.sent_to.split(",");

    contain = {
      id: doc.id,
      time: formatTime,
      reply_from: splitFrom[1],
      to: splitTo[1],
      last_message: doc.content_message,
    };

    return contain;
  });

  const countUnresd = await messages
    .findAndCountAll({
      where: {
        [Op.and]: [
          { sent_to: `${getUser.id},${getUser.name}` },
          { status: false },
        ],
      },
    })
    .then((result) => {
      return result.count;
    });

  const listAllMessagesIdHelper = {
    unread_count: countUnresd,
    data: mapData,
  };

  return listAllMessagesIdHelper;
};

module.exports = method;
