"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class messages extends Model {
    static associate({ users }) {
      // define association here
    }
  }
  messages.init(
    {
      date: DataTypes.DATE,
      sent_from: DataTypes.STRING,
      sent_to: DataTypes.STRING,
      content_message: DataTypes.TEXT,
      status: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "messages",
    }
  );
  return messages;
};
