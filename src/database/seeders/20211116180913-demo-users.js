"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "users",
      [
        {
          number: "085156960070",
          name: "Ahkam",
        },
        {
          number: "085156961171",
          name: "John Doe",
        },
        {
          number: "085156962272",
          name: "Jane Doe",
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
