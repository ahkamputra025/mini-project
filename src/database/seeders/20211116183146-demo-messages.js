"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "messages",
      [
        {
          date: new Date(),
          sent_from: "1,Ahkam",
          sent_to: "1,Ahkam",
          content_message: "Hai..?",
          status: false,
        },
        {
          date: new Date(),
          sent_from: "1,Ahkam",
          sent_to: "2,John Doe",
          content_message: "Hai Jhone..?",
          status: false,
        },
        {
          date: new Date(),
          sent_from: "1,Ahkam",
          sent_to: "3,Jane Doe",
          content_message: "Hai Jane..?",
          status: false,
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
