require("dotenv").config();
const express = require("express");
const cors = require("cors");

const app = express();
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const userRoutes = require("./src/routes/user.routes");
app.use("/api/", userRoutes);

app.get("/", (req, res) => res.send("Welcome to app API Karamin!"));
app.get("*", (req, res) =>
  res.send("You've tried reaching a route that doesn't exist.")
);

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server Running, mode on port ${port}`);
});

module.exports = app;
