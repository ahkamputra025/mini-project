# MINI PROJECT

Rakamin Recruitment
Backend Developer

## Getting started

1. Move yourself to the backend folder: `cd mini-project`
2. Copy the .env_example file and create a .env file and add the connection DB and other and SIGNATURE (can be any word)
3. Install sequelize-cli how dev-dependency and execute this `sequelize db:create` next `sequelize db:migrate` and `sequelize db:seed:all`
4. Install node-modules `$ npm i` and run with command `$ npm start`

# List API

Services API, deployed in heroku
| No | Routes | EndPoint | Description |
| -- | ------ | ------------------------------------------------------------- | -------------------------------------------------------- |
| 1. | POST | https://app-karamin25.herokuapp.com/api/sendMessage | Users can send a message to another user |
| 2. | GET | https://app-karamin25.herokuapp.com/api/listAllMessage | Users can list all messages in a conversation, |
| | | | between them and another user. |
| 3. | POST | https://app-karamin25.herokuapp.com/api/replyMessages | Users can reply to a conversation they are involved with.|
| 4. | GET | https://app-karamin25.herokuapp.com/api/listAllMessagesId/:id | User can list all their conversations |

Link doc postman : https://documenter.getpostman.com/view/17902429/UVJWpKBb
